package com.jonathan.jumia.javaexercise.controller;

import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jonathan.jumia.javaexercise.entity.Stats;
import com.jonathan.jumia.javaexercise.repository.StatsRepository;

@RestController
@RequestMapping("/card-scheme/stats")
public class StatsController {

	@Autowired
    private StatsRepository repository;
	
	@RequestMapping(path = "?start={start}&limit={limit}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Iterable<Stats>> getById(@PathVariable Long start, @PathVariable Long limit) throws URISyntaxException {
		try {
			
			return new ResponseEntity<>(repository.findAll(), HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	private Stats updateRegister(Stats item) {
		item.setCount(item.getCount() + 1);
		return repository.save(item);
	}
		
	
	public Stats saveRegister(String cardNumber) {
		Stats existing = checkExists(cardNumber);
		if(existing != null)
			return updateRegister(existing);
		else {
			Stats item = new Stats();
			item.setCardNumber(cardNumber);
			return repository.save(item);
		}
	}
	
	private Stats checkExists(String cardNumber) {
		Stats existing = repository.findByCardNumber(cardNumber);
		return existing;
	}
	
}
