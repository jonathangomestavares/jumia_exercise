package com.jonathan.jumia.javaexercise.controller;

import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;

//import javax.persistence.EntityNotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.jonathan.jumia.javaexercise.domain.Card;
import com.jonathan.jumia.javaexercise.domain.VerifyResponse;


@RestController
@RequestMapping("/card-scheme/verify")
public class VerifyController {
	
	final String uri = "https://lookup.binlist.net/";
	
	@Autowired
    private StatsController statsController;

	
	@RequestMapping(path = "/{cardNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<VerifyResponse> verifyCardNumber(@PathVariable String cardNumber) throws URISyntaxException {
		try {
			
			String verifyUrl = uri.concat(cardNumber);

		    RestTemplate restTemplate = new RestTemplate();
		    Card result = restTemplate.getForObject(verifyUrl, Card.class);
		    System.out.println(result);
		    //System.out.println(statsController.saveRegister(cardNumber));
		    populateResponse(result);

			return new ResponseEntity<>(populateResponse(result), HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(new VerifyResponse(), HttpStatus.OK);
		}
	}

	private VerifyResponse populateResponse(Card result) {
		VerifyResponse response = new VerifyResponse();
		response.setSuccess(true);
		response.setPayload(result);
		return response;
	}
}
