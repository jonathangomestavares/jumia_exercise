package com.jonathan.jumia.javaexercise.domain;

public class VerifyResponse {

	private boolean success = false;
	private Card payload;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Card getPayload() {
		return payload;
	}

	public void setPayload(Card payload) {
		this.payload = payload;
	}
}
