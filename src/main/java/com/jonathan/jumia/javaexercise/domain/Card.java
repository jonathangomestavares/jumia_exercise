package com.jonathan.jumia.javaexercise.domain;

import java.io.Serializable;

public class Card implements Serializable{
	
	private String scheme;
	private String type;
	private Bank bank;
	
	public String getScheme() {
		return scheme;
	}
	public void setScheme(String scheme) {
		this.scheme = scheme;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Bank getBank() {
		return bank;
	}
	public void setBank(Bank bank) {
		this.bank = bank;
	}

}
