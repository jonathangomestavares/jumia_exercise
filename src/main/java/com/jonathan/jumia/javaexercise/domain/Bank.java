package com.jonathan.jumia.javaexercise.domain;

import java.io.Serializable;

public class Bank implements Serializable{
	
	private String name;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
