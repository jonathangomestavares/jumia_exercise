package com.jonathan.jumia.javaexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaexerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaexerciseApplication.class, args);
	}

}
