package com.jonathan.jumia.javaexercise.repository;

import org.springframework.data.repository.CrudRepository;

import com.jonathan.jumia.javaexercise.entity.Stats;

public interface StatsRepository extends CrudRepository<Stats, Long> {
	
	Stats findByCardNumber(String cardNumber);
	
}
